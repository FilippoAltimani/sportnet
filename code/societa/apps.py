from django.apps import AppConfig


class SocietaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'societa'
