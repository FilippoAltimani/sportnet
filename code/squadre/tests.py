from django.test import TestCase
from django.utils import timezone
from datetime import timedelta
from .models import *


class GameTests(TestCase):
    def test_teams_being_same(self):
        team = Team(1,"team", 2000, "mister")
        team.save()
        game = Game(1,"1999-06-07 00:00:00","place", 0,0,team.id,team.id)
        self.assertRaises(ValidationError, game.save)
        

    def test_result_team1_winner(self):
        team1 = Team(1,"team1", 2000, "mister1")
        team2 = Team(2,"team2", 2000, "mister2")
        team1.save()
        team2.save()
        game = Game(1, "1999-06-07 00:00:00","place", 1,0,team1.id,team2.id)
        game.save()
        self.assertEqual(game.winner(),str(team1))

    def test_result_team2_winner(self):
        team1 = Team(1,"team1", 2000, "mister1")
        team2 = Team(2,"team2", 2000, "mister2")
        team1.save()
        team2.save()
        game = Game(1,"1999-06-07 00:00:00","place", 0,1,team1.id,team2.id)
        game.save()
        self.assertEqual(game.winner(),str(team2))

    def test_result_draw(self):
        team1 = Team(1,"team1", 2000, "mister1")
        team2 = Team(2,"team2", 2000, "mister2")
        team1.save()
        team2.save()
        game = Game(1,"1999-06-07 00:00:00","place", 0,0,team1.id,team2.id)
        game.save()
        self.assertEqual(game.winner(),"Pareggio")

