from django.contrib import admin
from django.urls import include, path, re_path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from .views import *
from .initcmds import *

urlpatterns = [
    path('admin/', admin.site.urls),

    re_path(r"^$|^/$|^home/$", home, name="home"),

    path('success/<str:back_url>', success_view, name="success"),

    path('register/', BasicUserCreateView.as_view(), name="register"),
    path('registersuper/',SuperUserCreateView.as_view(), name="registersuper"),
    path('login/', auth_views.LoginView.as_view(), name="login"),
    path('logout/', auth_views.LogoutView.as_view(), name="logout"),

    path('authorize/', AuthorizeUserView.as_view(), name="authorize"),
    path('approveuser/<pk>/', ApproveUserView.as_view(), name="approve"),

    path('squadre/', include('squadre.urls')),

    path('societa/', include('societa.urls')),

    path('bacheca/', include('bacheca.urls')),
    
] + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

init_db()