from django.urls import path
from .views import *


app_name = 'squadre'


urlpatterns = [
    path('', ListTeamsView.as_view(), name="teams"),
    path('rosa/<pk>/', DetailTeamView.as_view(), name="team"),

    path('giocatore/<pk>/', DetailPlayerView.as_view(), name="player"),

    path('partite/', search_games, name="games"),
    path('aggiungipartita/', CreateGameView.as_view(), name="creategame"),
    path('risultatopartita/<pk>/', ResGameView.as_view(), name="resgame"),
    path('convocazionepartita/<pk>/', ConGameView.as_view(), name="congame"),
    path('cancellapartita/<pk>/', DelGameView.as_view(), name="delgame"),
    
    path('squadrapreferita/<pk>/', SetPrefTeamView.as_view(), name="prefteam"),

    path('stampaconvocati/<int:id_game>/', print_convocation, name="printcov"),
    
]
