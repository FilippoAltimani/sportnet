from pathlib import Path
import societa.models as smodels


main_club_name = "Atletico Rolo"
main_club_city = "Rolo"
main_club_primary_color = "#126CF4"
main_club_secondary_color = "#eeeeee"


def init_db():
    if len(smodels.Club.objects.all()) == 0:
        c = smodels.Club()
        c.name = main_club_name
        c.city = main_club_city
        c.primary_color = main_club_primary_color
        c.secondary_color = main_club_secondary_color

        c.save()


def main_club():
    main_club_obj = smodels.Club.objects.get(name=main_club_name)
    if main_club_obj is None:
        init_db()
        main_club_obj = smodels.Club.objects.get(name=main_club_name)

    return main_club_obj


def club_media_path(club_name):
    return "club_" + club_name.replace(" ", "_")


def club_logo_directory_path(instance, filename):
    logo_and_format = "logo" +  Path(filename).suffix
    # TODO usare club_media_path(instance.user.club.name) nel caso ci siano più club
    return club_media_path(main_club_name)+"/{0}".format(logo_and_format)


def club_member_directory_path(instance, filename):
    # TODO usare club_media_path(instance.user.club.name) nel caso ci siano più club
    return club_media_path(main_club_name)+"/members/{0}".format(filename)


def team_directory_path(instance, filename):
    return club_media_path(main_club_name)+"/{0}/{1}".format(instance.team.year,filename)


def post_directory_path(instance, filename):
    return club_media_path(main_club_name)+"/{0}/{1}".format(instance.user.pk,filename)


def i_page_range(q_len:int, items_page_len:int, current_page:int):
    tot_page = q_len//items_page_len
    if q_len%items_page_len > 0:
        tot_page += 1
    if current_page > 1 and current_page <= tot_page:
        index_page_start = (current_page-1)*items_page_len
        index_page_end = current_page*items_page_len
    else:
        index_page_start = 0
        index_page_end = items_page_len
    return index_page_start, index_page_end, tot_page



class QueryForm():
    form = None
    q = None

    def __init__(self,query,form):
        # TODO veridica il tipo di form e query
        self.form = form
        self.q = query

    def query_order(self):
        order_date = self.form.cleaned_data.get("order_date")
        if order_date != None and order_date != "":
            order = order_date
        else:
            order = "-date"
        return order

    def query_search(self):
        search_date = self.form.cleaned_data.get("search_date")
        if search_date != None:
            self.q = self.q.filter(date__date=search_date)
        return self.q
