from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from .models import *


class PostTests(TestCase):

    def test_post_not_visible_if_not_approved(self):
        user = User(1,"psw",timezone.now(),0,"usr")
        user.save()
        post = Post(1,"desc",timezone.now(),1,False)
        post.save()
        response = self.client.get(reverse('bacheca:posts'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['object_list'], [ ])
    
    def test_post_visible_if_approved(self):
        user = User(1,"psw",timezone.now(),0,"usr")
        user.save()
        post = Post(1,"desc",timezone.now(),1,True)
        post.save()
        response = self.client.get(reverse('bacheca:posts'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['object_list'], [post])
