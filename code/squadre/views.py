from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from braces.views import GroupRequiredMixin, UserPassesTestMixin
from sportnet.initcmds import i_page_range
from .squadrecmds import QueryGameForm
from .models import *
from .forms import *


game_page_len = 5


def print_convocation(request, id_game):
    ctx = {}
    try:
        id_g = int(id_game)
        game = Game.objects.get(id=id_g)
        players = game.players.all()
        ctx["object_list"] = players
        ctx["game"] = game
    except:
        ctx["object_list"] = []
        ctx["game"] = ""
    return render(request, template_name="squadre/convocationDoc.html", context=ctx)


class ListTeamsView(ListView):
    model = Team
    template_name = "squadre/teams.html"
    title = "Squadre"


class DetailTeamView(DetailView):
    model = Team
    template_name = "squadre/team.html"


class DetailPlayerView(DetailView):
    model = Player
    template_name = "squadre/player.html"


def search_games(request):
    ctx = {}
    q = Game.objects.all()
    form = SearchGamesForm()
    page_num = 1
    order = "-date"

    if request.method == "POST":
        form = SearchGamesForm(request.POST)
        if form.is_valid():
            qpg = QueryGameForm(q, form)
            q = qpg.query_search()
            order = qpa.query_order()

            try: page_num = int(request.POST["page"])
            except: page_num = 1

    i_page_start, i_page_end, tot_page = i_page_range(len(q), game_page_len, page_num)
    q = q.order_by(order)

    ctx["object_list"] = q[i_page_start:i_page_end]
    ctx["page"] = page_num
    ctx["range"] = range(1,tot_page+1)
    ctx["form"] = form
    return render(request, template_name="squadre/games.html", context=ctx)

class ListGamesView(ListView):
    model = Game
    template_name = "squadre/games.html"
    title = "Partite"


class CreateGameView(GroupRequiredMixin,CreateView):
    model = Game
    template_name = "modObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "squadre:games"})
    fields = ["date","place","team1","points_team1", "team2","points_team2"]
    group_required = ["Super"]
    title = "Aggiungi partita"

class ResGameView(GroupRequiredMixin,UpdateView):
    model = Game
    template_name = "modObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "squadre:games"})
    fields = ["points_team1", "points_team2"]
    group_required = ["Super"]
    title = "Risultato partita"


class ConGameView(GroupRequiredMixin,UpdateView):
    model = Game
    template_name = "modObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "squadre:games"})
    fields = ["players"]
    group_required = ["Super"]
    title = "Convicazione partita"


class DelGameView(GroupRequiredMixin,DeleteView):
    model = Game
    template_name = "delObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "squadre:games"})
    group_required = ["Super"]
    title = "Elimina partita"


class SetPrefTeamView(UserPassesTestMixin, UpdateView):
    model = PrefTeam
    template_name = "modObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "home"})
    fields = ["team"]
    title = "Scegli squadra preferita"
    
    def test_func(self, user): 
        return (user.pk == self.get_object().user.pk or user.is_staff)