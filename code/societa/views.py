from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin
from .models import *
from .societacmds import *
from sportnet.initcmds import main_club


def adminClub(request):
    response = redirect("societa:members")
    get_querydict = request.GET.copy()
    try:
        item = get_querydict.popitem()
        int(item[1][0]) # se non causa un eccezzione allora è un intero
        if item[0] in accepted_path:
            response = redirect("societa:"+item[0], item[1][0])
    except KeyError:
        print("adminClub: WRONG PARAM")
    except ValueError:
        print("adminClub: NOT INT")
    except:
        print("adminClub: EXCEPTION UNKNOW")
    
    return response


class ListMemberView(ListView):
    model = Member
    template_name = "societa/members.html"
    title = "Organigramma"

    def get_queryset(self):
        # TODO filtro per club in base all'utente loggato 
        return self.model.objects.filter(club=main_club().pk)


class ListSponsorView(ListView):
    model = Sponsor
    template_name = "societa/sponsors.html"
    title = "Sponsor"

    def get_queryset(self):
        # TODO filtro per club in base all'utente loggato
        return self.model.objects.raw(
            """
            SELECT * FROM societa_sponsor
            INNER JOIN societa_financing ON societa_sponsor.id = societa_financing.sponsor_id
            WHERE societa_financing.club_id = 
            """
            + str(main_club().pk)
        )


class CreateMemberView(PermissionRequiredMixin, CreateView):
    model = Member
    template_name = "modObj.html"
    fields = member_mod_fields
    success_url = reverse_lazy("success", kwargs={"back_url": "societa:members"})
    title = "Aggiungi socio"
    permission_required = "is_staff"

    def form_valid(self, form):
        # TODO metti il club in base all'utente loggato
        form.instance.club = main_club()
        return super().form_valid(form)


class CreateSponsorView(PermissionRequiredMixin, CreateView):
    model = Sponsor
    template_name = "modObj.html"
    fields = sponsor_mod_fields
    success_url = reverse_lazy("success", kwargs={"back_url": "societa:sponsors"})
    title = "Aggiungi sponsor"
    permission_required = "is_staff"
    
    def form_valid(self, form):
        response = super().form_valid(form)
        if self.object is not None:
            bind = Financing()
            bind.club = main_club()
            bind.sponsor = self.object
            bind.save()
        return response


class DelMemberView(PermissionRequiredMixin, DeleteView):
    model = Member
    template_name = "delObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "societa:members"})
    title = "Cancella socio"
    permission_required = "is_staff"


class DelSponsorView(PermissionRequiredMixin, DeleteView):
    model = Sponsor
    template_name = "delObj.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "societa:sponsors"})
    title = "Cancella sponsor"
    permission_required = "is_staff"


class ModMemberView(PermissionRequiredMixin, UpdateView):
    model = Member
    template_name = "modObj.html"
    fields = member_mod_fields
    success_url = reverse_lazy("success", kwargs={"back_url": "societa:members"})
    title = "Modifica socio"
    permission_required = "is_staff"


class ModSponsorView(PermissionRequiredMixin, UpdateView):
    model = Sponsor
    template_name = "modObj.html"
    fields = sponsor_mod_fields
    success_url = reverse_lazy("success", kwargs={"back_url": "societa:sponsors"})
    title = "Modifica sponsor"
    permission_required = "is_staff"

