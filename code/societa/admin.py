from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Club)
admin.site.register(Member)
admin.site.register(Sponsor)
admin.site.register(Financing)
admin.site.register(Usership)
