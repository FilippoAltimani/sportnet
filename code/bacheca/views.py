from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import PermissionRequiredMixin
from braces.views import GroupRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from sportnet.initcmds import i_page_range
from .models import *
from .forms import *
from .bachecacmds import QueryPostsForm, QueryAnnouncementsForm


post_page_len = 5
ann_page_len = 5

class AuthorizePostView(ListView):
    model = Post
    template_name = "bacheca/authPosts.html"
    title = "Autorizza post"

    def get_queryset(self):
        print(Post.objects.filter(approved=False))
        return Post.objects.filter(approved=False)


class ApprovePostView(PermissionRequiredMixin, UpdateView):
    model = Post
    template_name = "bacheca/apprPost.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:authorizepost"})
    title = "Approva post"
    fields = []
    permission_required = "is_staff"

    def form_valid(self, form):
        self.object.approved = True
        return super().form_valid(form)


def search_posts(request):
    ctx = {}
    q = Post.objects.all().filter(approved=True)
    form = SearchPostsForm()
    page_num = 1
    order = "-date"

    if request.method == "POST":
        form = SearchPostsForm(request.POST)
        if form.is_valid():
            qpf = QueryPostsForm(q, form)
            q = qpf.query_search()
            order = qpf.query_order()

            try: page_num = int(request.POST["page"])
            except: page_num = 1

    i_page_start, i_page_end, tot_page = i_page_range(len(q), post_page_len, page_num)
    q = q.order_by(order)

    ctx["object_list"] = q[i_page_start:i_page_end]
    ctx["page"] = page_num
    ctx["range"] = range(1,tot_page+1)
    ctx["form"] = form
    return render(request, template_name="bacheca/posts.html", context=ctx)


def search_announcements(request):
    ctx = {}
    q = Announcement.objects.all()
    form = SearchAnnouncemenForm()
    page_num = 1
    order = "-date"

    if request.method == "POST":
        form = SearchAnnouncemenForm(request.POST)
        if form.is_valid():
            qpa = QueryAnnouncementsForm(q, form)
            q = qpa.query_search()
            order = qpa.query_order()

            try: page_num = int(request.POST["page"])
            except: page_num = 1

    i_page_start, i_page_end, tot_page = i_page_range(len(q), ann_page_len, page_num)
    q = q.order_by(order)

    ctx["object_list"] = q[i_page_start:i_page_end]
    ctx["page"] = page_num
    ctx["range"] = range(1,tot_page+1)
    ctx["form"] = form
    return render(request, template_name="bacheca/anns.html", context=ctx)


def like_post(request, id_post):
    response = False
    if request.user.is_authenticated:
        try:
            id_p = int(id_post)
            likes = LikeImage.objects.filter(user__pk=request.user.pk)
            likes = likes.filter(post__pk=id_p)
            if len(likes) == 0:
                like = LikeImage()
                like.user = request.user
                like.post = Post.objects.get(pk=id_p)
                like.save()
                response = True
        except: response = False
    return HttpResponse(response)


def unlike_post(request, id_post):
    response = False
    if request.user.is_authenticated:
        try:
            id_p = int(id_post)
            likes = LikeImage.objects.filter(user__pk=request.user.pk)
            likes = likes.filter(post__pk=id_p)
            for like in likes:
                like.delete()
                response = True
            likes = LikeImage.objects.filter(user__pk=request.user.pk)
            likes = likes.filter(post__pk=id_p)
        except: response = False
    return HttpResponse(response)


class DetailPostView(DetailView):
    model = Post
    template_name = "bacheca/post.html"


class DetailAnnouncementView(DetailView):
    model = Announcement
    template_name = "bacheca/ann.html"


class CreateCommentView(GroupRequiredMixin, CreateView):
    template_name = "modObj.html"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class CreatePostView(CreateCommentView):
    model = Post
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:posts"})
    fields = ["desc","image","team","game","players"]
    group_required = ["Approved"]
    title = "Pubblica post"
    def form_valid(self, form):
        if self.request.user.is_staff:
            form.instance.approved = True
        return super().form_valid(form)


class CreateAnnouncementView(CreateCommentView):
    model = Announcement
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:announcements"})
    fields = ["title","desc","teams"]
    group_required = ["Super"]
    title = "Pubblica annuncio"


class CreateCommPostView(CreateCommentView):
    model = PostComment
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:posts"})
    fields = ["desc"]
    group_required = ["Approved"]
    title = "Commenta post"

    def form_valid(self, form):
        p = Post.objects.get(pk=self.kwargs["id_post"])
        form.instance.post = p
        return super().form_valid(form)


class CreateCommAnnouncementView(CreateCommentView):
    model = AnnouncementComment
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:announcements"})
    fields = ["desc"]
    group_required = ["Approved"]
    title = "Commenta annuncio"

    def form_valid(self, form):
        a = Announcement.objects.get(pk=self.kwargs["id_ann"])
        form.instance.announcement = a
        return super().form_valid(form)


class DelCommentView(UserPassesTestMixin, DeleteView):
    template_name = "delObj.html"
    def test_func(self, user): 
        return (user.pk == self.get_object().user.pk or user.is_staff)

class DelPostView(DelCommentView):
    model = Post
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:posts"})
    title = "Cancella post"


class DelAnnouncementView(DelCommentView):
    model = Announcement
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:announcements"})
    title = "Cancella annuncio"


class DelCommPostView(DelCommentView):
    model = PostComment
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:posts"})
    title = "Cancella commento post"


class DelCommAnnouncementView(DelCommentView):
    model = AnnouncementComment
    success_url = reverse_lazy("success", kwargs={"back_url": "bacheca:announcements"})
    title = "Cancella commento annuncio"