from django import forms
from django.conf import settings
from .models import *
from django.contrib.auth.models import User


class SearchCommentsForm(forms.Form):
    search_date = forms.DateField(
        label="Cerca post pubblicato in data:",
        required=False
    )
    search_user = forms.ModelChoiceField(
        queryset= User.objects.all(),
        required=False,
        label="Cerca per utente"
    )
    search_desc = forms.CharField(
        label="Cerca parole nella descrizione",
        max_length=200,
        required=False
    )
    search_team = forms.ModelChoiceField(
        queryset=Team.objects.all(),
        required=False,
        label="Cerca una squadra"
    )
    order_date = forms.ChoiceField(
        label="Ordina per data",
        required=False,
        choices=[
            ("-date","Dalla pubblicazione più recente(Default)"),
            ("date","Dalla pubblicazione meno recente")
        ],
        widget=forms.RadioSelect,
    )


class SearchPostsForm(SearchCommentsForm):
    search_game = forms.ModelChoiceField(
        queryset=Game.objects.all(),
        required=False,
        label="Cerca una partita"
    )
    search_player = forms.ModelChoiceField(
        queryset=Player.objects.all(),
        required=False,
        label="Cerca un giocatore"
    )


class SearchAnnouncemenForm(SearchCommentsForm):
    search_title = forms.CharField(
        label="Cerca parole nella descrizione",
        max_length=50,
        required=False
    )

