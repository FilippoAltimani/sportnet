from django import forms
from .models import *

class SearchGamesForm(forms.Form):
    search_date = forms.DateField(
        label="Cerca una partita in data:",
        required=False
    )
    search_place = forms.CharField(
        label="Cerca una partita nel luogo:",
        max_length=200,
        required=False
    )
    search_team = forms.ModelChoiceField(
        queryset=Team.objects.all(),
        required=False,
        label="Cerca una squadra"
    )
    search_team_result = forms.ChoiceField(
        label="Che risultato ha ottenuto la squadra?",
        required=False,
        choices=[
            ("Qualsiasi","Qualsiasi risultato"),
            ("Vittoria","Hanno vinto"),
            ("Sconfitta","Hanno perso"),
            ("Pareggio","Hanno pareggiato")
        ],
        widget=forms.RadioSelect,
    )
    search_player = forms.ModelChoiceField(
        queryset=Player.objects.all(),
        required=False,
        label="Cerca un giocatore"
    )
    order_date = forms.ChoiceField(
        label="Ordina per data",
        required=False,
        choices=[
            ("-date","Dalla più recente(Default)"),
            ("date","Dalla meno recente")
        ],
        widget=forms.RadioSelect,
    )