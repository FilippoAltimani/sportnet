member_mod_fields = [
    "name",
    "surname",
    "role",
    "mail",
    "picture",
]

sponsor_mod_fields = [
    "name",
    "description",
    "website",
    "logo"
]

accepted_path = (
    "delmember",
    "modmember",
    "delsponsor",
    "modsponsor"
)