from django.db import models
from societa.models import Club
from sportnet.initcmds import team_directory_path
from django.core.exceptions import ValidationError
from django.conf import settings
from django.utils import timezone


class Team(models.Model):
    name = models.CharField(max_length=50, default="")
    year = models.CharField(max_length=4)
    mister = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        default=None
    )
    club = models.ForeignKey(
        Club,
        null=True,
        blank=True,
        default=None,
        on_delete=models.CASCADE,
        related_name='train',
    )

    def __str__(self):
        return self.name + "(" +str(self.year) + ")"


class Player(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.DateField()
    picture = models.FileField(
        upload_to=team_directory_path,
        null=True,
        blank=True,
        default=None,
    )
    team = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name='play'
    )

    def __str__(self):
        return self.first_name + " " + self.last_name

class Game(models.Model):
    date = models.DateTimeField()
    place = models.CharField(max_length=200)
    points_team1 = models.IntegerField(default=0)
    points_team2 = models.IntegerField(default=0)
    team1 = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name='participate_h',
    )
    team2 = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name='participate_a',
    )

    players = models.ManyToManyField(
        Player,
        blank=True,
        default=None,
        db_table = 'convocation',
        related_name='convocated'
    )

    def clean(self):
        if self.team1.id == self.team2.id:
            raise ValidationError("Team cannot play against itself")
        if self.id != None:
            for player in self.players.all():
                if player.team.pk != self.team1.pk and player.team.pk != self.team2.pk:
                    self.players.remove(player)
                    raise ValidationError("Player must play for one of the 2 teams")

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def winner(self):
        winner_team = "Pareggio"
        if self.points_team1 > self.points_team2:
            winner_team = self.team1
        elif self.points_team2 > self.points_team1:
            winner_team = self.team2
        return str(winner_team)

    def __str__(self):
        return str(self.team1) + " VS " +str(self.team2)


class PrefTeam(models.Model):
    team = models.ForeignKey(
        Team,
        null=True,
        blank=True,
        default=None,
        on_delete=models.CASCADE,
        related_name='is_preferred'
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='follow',
        primary_key=True
    )