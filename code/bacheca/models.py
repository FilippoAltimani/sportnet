from django.db import models
from sportnet.initcmds import post_directory_path
from django.conf import settings
from squadre.models import Team, Player, Game
from django.utils import timezone


class Comment(models.Model):
    desc = models.TextField(max_length=500)
    date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    class Meta:
        abstract = True


class Post(Comment):
    approved = models.BooleanField(default=False)
    image = models.ImageField(
        upload_to=post_directory_path,
        null=True,
        blank=True,
        default=None
    )
    team = models.ForeignKey(
        Team,
        null=True,
        blank=True,
        default=None,
        on_delete=models.PROTECT,
        related_name='reference'
    )
    game = models.ForeignKey(
        Game,
        null=True,
        blank=True,
        default=None,
        on_delete=models.PROTECT,
        related_name='reference'
    )
    players = models.ManyToManyField(
        Player,
        blank=True,
        default=None,
        related_name='tagged'
    )
    def __str__(self):
        return str(self.user.username) + " post " + str(self.pk)


class Announcement(Comment):
    title = models.CharField(max_length=50)
    teams = models.ManyToManyField(
        Team,
        blank=True,
        default=None,
        related_name='ann_reference',
    )
    def __str__(self):
        return self.title

class PostComment(Comment):
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='comments'
    )


class AnnouncementComment(Comment):
    announcement = models.ForeignKey(
        Announcement,
        on_delete=models.CASCADE,
        related_name='comments'
    )


class LikeImage(models.Model):
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='is_liked'
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='like_a'
    )
    def __str__(self):
        return self.user.username + " like " + str(self.post)