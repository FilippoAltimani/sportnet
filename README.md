Altimani Filippo
Progetto di tecnologie web

[Software richiest]
Python v3.12 +
Pip
Pipenv
git (facoltativo)

[Comandi per linux] 
git clone https://gitlab.com/FilippoAltimani/sportnet.git
cd sportnet/code
python -m pipenv sync
python -m pipenv install
python -m pipenv shell
python manage.py runserver
