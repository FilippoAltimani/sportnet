from django.contrib import admin
from .models import *


admin.site.register(Post)
admin.site.register(Announcement)
admin.site.register(PostComment)
admin.site.register(AnnouncementComment)
admin.site.register(LikeImage)

