from django.db import models
from django.conf import settings
from sportnet.initcmds import club_logo_directory_path, club_member_directory_path


class Club(models.Model):
    name = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    primary_color = models.CharField(max_length=7)
    secondary_color = models.CharField(max_length=7)
    logo = models.ImageField(
        upload_to=club_logo_directory_path,
        null=True,
        blank=True,
        default=None,
    )

    def __str__(self):
        return self.name


class Member(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    role = models.CharField(max_length=50)
    mail = models.EmailField()
    picture = models.ImageField(
        upload_to=club_member_directory_path,
        null=True,
        blank=True,
        default=None,
    )
    club = models.ForeignKey(
        Club,
        on_delete=models.CASCADE,
        related_name='management'
    )

    def __str__(self):
        return self.name + " " + self.surname


class Sponsor(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200) # TODO TextField
    website = models.URLField()
    logo = models.ImageField(
        upload_to="sponsors/",
        null=True,
        blank=True,
        default=None,
    )

    def __str__(self):
        return self.name


class Financing(models.Model):
    club = models.ForeignKey(
        Club,
        on_delete=models.CASCADE,
        related_name='is_finance'
    )
    sponsor = models.ForeignKey(
        Sponsor,
        on_delete=models.CASCADE,
        related_name='finance'
    )

    def __str__(self):
        return self.club.name + " is finance by " + self.sponsor.name


class Usership(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    club = models.ForeignKey(
        Club,
        on_delete=models.CASCADE,
        related_name='belongs_to'
    )
