from django.contrib.auth.models import Group, User
from django.contrib.auth.forms import UserCreationForm
from django import forms 
from societa.models import Usership
from squadre.models import PrefTeam
from .initcmds import *


class BasicUserCreationForm(UserCreationForm):
    groups = ["Basic"]

    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "password1", "password2")
    
    def save(self, commit=True):
        user = super().save(commit)
        for g in self.groups:
            g = Group.objects.get(name=g)
            g.user_set.add(user)

        us = Usership()
        us.user = user
        us.club = main_club()
        us.save()

        pt = PrefTeam()
        pt.user = user
        pt.save

        return user


class SuperUserCreationForm(BasicUserCreationForm):
    groups = ["Approved", "Super"]
