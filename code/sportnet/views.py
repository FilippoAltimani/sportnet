from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy
from django.utils import timezone
from .forms import *
from bacheca.models import Post
from squadre.models import Game


num_img_slideshow = 5
num_games_calendar = 5

def home(request):
    ctx = {}
    posts = Post.objects.filter(approved=True)
    posts = posts.exclude(image__isnull=True)
    posts = posts.exclude(image__iexact="")
    posts = posts.order_by("-date")[:num_img_slideshow]
    images = []
    for p in posts:
        images.append(p.image.url)

    games = Game.objects.filter(date__gte=timezone.now())
    if request.user.is_authenticated:
        if request.user.follow.team != None:
            team_followed = request.user.follow.team.id
            games1 = games.filter(team1__pk=team_followed)
            games2 = games.filter(team2__pk=team_followed)
            games = games1 | games2

        likes = request.user.like_a.all()
        likes = likes.exclude(post__image__isnull=True)
        likes = likes.exclude(post__image__iexact="")
        likes = likes.order_by("post__date")[:num_img_slideshow]
        if len(likes) > 0:
            images = []
            for l in likes:
                images.append(l.post.image.url)

    range_len = len(images) 
    ctx["range"] = range(1,range_len+1)
    ctx["images"] = images
    ctx["games"] = games.order_by("date")[:num_games_calendar]
        
    return render(request, template_name="home.html", context=ctx)


def success_view(request,back_url:str):
    return render(request, template_name="success.html",context={"back_url":back_url})

class BasicUserCreateView(CreateView):
    form_class = BasicUserCreationForm
    template_name = "user_create.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "login"})


class SuperUserCreateView(PermissionRequiredMixin, BasicUserCreateView):
    permission_required = "is_staff"
    form_class = SuperUserCreationForm


class AuthorizeUserView(PermissionRequiredMixin, ListView):
    model = User
    template_name = "authUsrs.html"
    permission_required = "is_staff"
    title = "Autorizza utenti"

    def get_queryset(self):
        return User.objects.filter(groups__name='Basic').exclude(groups__name='Approved')


class ApproveUserView(PermissionRequiredMixin, UpdateView):
    model = User
    template_name = "apprUsr.html"
    success_url = reverse_lazy("success", kwargs={"back_url": "authorize"})
    title = "Approva utente"
    fields = []
    permission_required = "is_staff"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            usr_id = int(context["object"].pk)
            usr = User.objects.get(id=usr_id)
            context['behavior'] = str(usr.groups.filter(name='Approved').exists())
        except:
            context['behavior'] = "error"
        return context

    def form_valid(self, form):
        group_approved = Group.objects.get(name='Approved')
        group_approved.user_set.add(self.object)
        return super().form_valid(form)