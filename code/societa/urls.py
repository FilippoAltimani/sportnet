from django.urls import path
from .views import *

app_name = 'societa'

urlpatterns = [  
    path('soci/', ListMemberView.as_view(), name="members"),
    path('sponsor/', ListSponsorView.as_view(), name="sponsors"),

    path('aggiungisocio/', CreateMemberView.as_view(), name="createmember"),
    path('aggiungisponsor/', CreateSponsorView.as_view(), name="createsponsor"),

    path('amministra/', adminClub, name="adminclub"),

    path('cancellasocio/<pk>/', DelMemberView.as_view(), name="delmember"),
    path('modificasocio/<pk>/', ModMemberView.as_view(), name="modmember"),

    path('cencellasponsor/<pk>/', DelSponsorView.as_view(), name="delsponsor"),
    path('modificasponsor/<pk>/', ModSponsorView.as_view(), name="modsponsor"),
]
