from sportnet.initcmds import QueryForm

class QueryCommentsForm(QueryForm):
    def query_search(self):
        super().query_search()

        search_user = self.form.cleaned_data.get("search_user")
        search_desc = self.form.cleaned_data.get("search_desc")
        
        if search_user != None:
            self.q = self.q.filter(user__pk=search_user.pk)

        if search_desc != None:
            self.q = self.q.filter(desc__icontains=search_desc)
        
        return self.q


class QueryPostsForm(QueryCommentsForm):
    def query_search(self):
        super().query_search()

        search_game = self.form.cleaned_data.get("search_game")
        search_player = self.form.cleaned_data.get("search_player")
        search_team = self.form.cleaned_data.get("search_team")

        if search_game != None:
            self.q = self.q.filter(game__pk=search_game.pk)
        if search_player != None:
            self.q = self.q.filter(players__pk=search_player.pk)
        if search_team != None:
            qt1 = self.q.filter(team__pk=search_team.pk)

        return self.q


class QueryAnnouncementsForm(QueryCommentsForm):
    def query_posts(self):
        super().query_search()

        search_title = self.form.cleaned_data.get("search_title")
        search_team = self.form.cleaned_data.get("search_team")

        if search_title != None:
            self.q = self.q.filter(title__icontains=search_title)
        if search_team != None:
            qt1 = self.q.filter(teams__pk=search_team.pk)

        return self.q