from sportnet.initcmds import QueryForm


class QueryGameForm(QueryForm):
    def query_search(self):
        super().query_search()
        search_player = self.form.cleaned_data.get("search_player")
        search_place = self.form.cleaned_data.get("search_place")
        search_team = self.form.cleaned_data.get("search_team")
        search_team_result = self.form.cleaned_data.get("search_team_result")

        if search_place != None:
            self.q = self.q.filter(place__icontains=search_place)
        if search_team != None:
            qt1 = self.q.filter(team1__pk=search_team.pk)
            qt2 = self.q.filter(team2__pk=search_team.pk)
            self.q = qt1|qt2
            if search_team_result == "Vittoria":
                for l in self.q:
                    if l.winner() == "Pareggio" or l.winner() != str(search_team):
                        self.q = self.q.exclude(pk=l.pk)
            elif search_team_result == "Sconfitta":
                for l in self.q:
                    if l.winner() == "Pareggio" or l.winner() == str(search_team):
                        self.q = self.q.exclude(pk=l.pk)
            elif search_team_result == "Pareggio":
                for l in self.q:
                    if l.winner() != "Pareggio":
                        self.q = self.q.exclude(pk=l.pk)
        if search_player != None:
            self.q = self.q.filter(players__pk=search_player.pk)
        return self.q