from django.urls import path, re_path
from .views import *

app_name = 'bacheca'

urlpatterns = [  
    path('posts/', search_posts, name="posts"),
    path('annunci/', search_announcements, name="announcements"),

    path('post/<pk>/', DetailPostView.as_view(), name="post"),
    path('annuncio/<pk>/', DetailAnnouncementView.as_view(), name="announcement"),

    path('aggiungipost/', CreatePostView.as_view(), name="addpost"),
    path('aggiungiannuncio/', CreateAnnouncementView.as_view(), name="addannouncement"),

    path('authorizepost/', AuthorizePostView.as_view(), name="authorizepost"),
    path('approvepost/<pk>/', ApprovePostView.as_view(), name="approvepost"),

    path('commentapost/<int:id_post>/', CreateCommPostView.as_view(), name="addcommpost"),
    path('commentaannuncio/<int:id_ann>/', CreateCommAnnouncementView.as_view(), name="addcommannouncement"),

    path('eliminapost/<pk>/', DelPostView.as_view(), name="delpost"),
    path('eliminaannuncio/<pk>/', DelAnnouncementView.as_view(), name="delannouncement"),

    path('eliminacommentopost/<int:id_post>/<pk>/', DelCommPostView.as_view(), name="delcommpost"),
    path('eliminacommentoannuncio/<int:id_ann>/<pk>/', DelCommAnnouncementView.as_view(), name="delcommannouncement"),

    path('likepost/<int:id_post>/', like_post, name="likepost"),
    path('unlikepost/<int:id_post>/', unlike_post, name="unlikepost"),
]